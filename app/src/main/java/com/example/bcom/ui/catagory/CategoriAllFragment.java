package com.example.bcom.ui.catagory;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bcom.R;
import com.example.bcom.model.Categorymodel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class CategoriAllFragment extends Fragment implements CategoriAdapter.ItemAdapterCallback{
private RecyclerView rvCategori;
private List<Categorymodel> categorymodelList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_categori_all, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvCategori = view.findViewById(R.id.rv_categori);
    }

    @Override
    public void onActivityCreated(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        categorymodelList = new ArrayList<>();
        categorymodelList.add(new Categorymodel(1,"All"));
        categorymodelList.add(new Categorymodel(1,"Sport"));
        categorymodelList.add(new Categorymodel(1,"Casual"));

        CategoriAdapter categoriAdapter = new CategoriAdapter(categorymodelList, this);
        rvCategori.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL, false));
        rvCategori.setAdapter(categoriAdapter);
    }


    @Override
    public void onClick(View view) {

    }
}