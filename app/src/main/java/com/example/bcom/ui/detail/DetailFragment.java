package com.example.bcom.ui.detail;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.bcom.R;
import com.example.bcom.model.Colorsmodel;
import com.example.bcom.model.Sizemodel;
import com.example.bcom.ui.cart.CartActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class DetailFragment extends Fragment implements ColorsAdapter.ItemAdapterCallback, SizeAdapter.ItemAdapterCallback {

    private RecyclerView rvcolor;
    private List<Colorsmodel> colorsmodelList;
    private RecyclerView rvSize;
    private List<Sizemodel> sizemodelList;
    private Button buttonAddtoCart;


    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvcolor = view.findViewById(R.id.recyclerView);
        rvSize = view.findViewById(R.id.recyclerView2);
        buttonAddtoCart = view.findViewById(R.id.button);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        colorsmodelList = new ArrayList<>();
        colorsmodelList.add(new Colorsmodel(1,R.color.colorAccent));
        colorsmodelList.add(new Colorsmodel(2,R.color.colorGrey));
        colorsmodelList.add(new Colorsmodel(3,R.color.colorPrimary));

        ColorsAdapter colorsAdapter = new ColorsAdapter(colorsmodelList, this);
        rvcolor.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));
        rvcolor.setAdapter(colorsAdapter);

        sizemodelList = new ArrayList<>();
        sizemodelList.add(new Sizemodel(1,"36\nEU"));
        sizemodelList.add(new Sizemodel(2,"37\nEU"));
        sizemodelList.add(new Sizemodel(3,"38\nEU"));
        sizemodelList.add(new Sizemodel(4,"39\nEU"));
        sizemodelList.add(new Sizemodel(5,"40\nEU"));
        sizemodelList.add(new Sizemodel(6,"41\nEU"));
        sizemodelList.add(new Sizemodel(7,"42\nEU"));
        sizemodelList.add(new Sizemodel(8,"43\nEU"));


        SizeAdapter sizeAdapter = new SizeAdapter(sizemodelList, this);
        rvSize.setLayoutManager(new GridLayoutManager(getContext(), 4));
        rvSize.setAdapter(sizeAdapter);

        buttonAddtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goCart = new Intent(getActivity(), CartActivity.class);
                startActivity(goCart);
            }
        });

    }

    @Override
    public void onClick(View view) {

    }
}