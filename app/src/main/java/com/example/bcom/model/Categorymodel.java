package com.example.bcom.model;

public class Categorymodel {
    private int id;
    private String labels;

    public Categorymodel(int id, String labels) {
        this.id = id;
        this.labels = labels;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getlabels() {
        return labels;
    }

    public void setlabels(String labels) {
        this.labels = labels;
    }
}


